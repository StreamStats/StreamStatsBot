'use strict';

exports.commands = {
	"ping": {
		process: function(bot, message) {
			bot.sendMessage(message.channel, "PONG");
		}
	},


	"hello": {
		process: function(bot, message) {
			bot.sendMessage(message.channel, "```Hello there! I am "+bot.user.username +", a bot made entirely by the Bacon_Space!```")
		}
	},
	"beamstats": {
		process: function(bot, message) {
			bot.sendMessage(message.channel, "```Here you Can Get Beamstats at This link```http://streamstats.github.io/BeamStats")
		}
	},
	
	"gitstats": {
		process: function(bot, message) {
			bot.sendMessage(message.channel, "```Here you Can Get GitStats at This link```http://streamstats.github.io/GitStats")
		}
	},
	"help": {
		process: function(bot, message) {
			bot.sendMessage(message.channel, "Currently, I am in prealpha stages, and all I can do is respond to ping, beamstats, gitstats, and help.")
		}
	}
};